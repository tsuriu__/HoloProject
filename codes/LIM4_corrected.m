#!/usr/bin/octave
%--------------------------LIM4-------------------------
% Function: the reconstruction of a hologram using the S-FFT
% algorithm
%
% Procedure: (l) Read an image file
%
%
% Variables:
% Ih : hologram;
% h : wavelength (mm);
% L : width of the hologram (mm);
% L0 : width of the diffracted object field (mm);
% z0 : reconstruction distance (mm);
% U0 : complex amplitude in the reconstruction plane;
%------------------------------------------------------------
clear;clc;close all;
pkg load image;

[nom,chemin]=uigetfile('../images/');
I1=imread([chemin,nom]);
figure;imagesc(I1);colormap(gray);axis equal;axis tight;title('Digital hologram');
Ih1=double(I1)-mean2(double(I1));
[N1,N2]=size(Ih1);
N=min(N1,N2); % Restriction to NxN
Ih=Ih1(1:N,1:N);
pix=input('Pixel pitch (mm) : ');
h=input('Wavelength (mm) : ');
z0=input('Reconstruction distance z0 (+ for a real image, - for a virtual image)(mm) : ');
L=pix*N;

%------------------------Reconstruction by S-FFT

n=-N/2:N/2-1;
x=n*pix;y=x;
[xx,yy]=meshgrid(x,y);
k=2*pi/h;
Fresnel=exp(i*k/2/z0*(xx.^2+yy.^2));
f2=Ih.*Fresnel;
Uf=fft2(f2,N,N);
Uf=fftshift(Uf);
ipix=h*abs(z0)/N/pix;
x=n*ipix;
y=x;
[xx,yy]=meshgrid(x,y);
phase=exp(i*k*z0)/(i*h*z0)*exp(i*k/2/z0*(xx.^2+yy.^2));
U0=Uf.*phase;

%-------------------------End of S-FFT

If=abs(U0).^0.75;
Gmax=max(max(If));
Gmin=min(min(If));
L0=abs(h*z0*N/L);
disp(['Width of the reconstruction plane =',num2str(L0),' mm']);
figure;imagesc(If,[Gmin,Gmax]),colormap(gray);axis('equal');axis('tight');ylabel('pixels');
xlabel(['Width of the reconstruction plane =',num2str(L),' mm']);
title('Image reconstructed by S-FFT');
p=input('Display parameter (>1) : ');
while isempty(p) == 0
	imagesc(If,[Gmin Gmax/p]),colormap(gray);axis('equal');axis('tight');ylabel('pixels');
	xlabel(['Width of the reconstruction plane =',num2str(L),' mm']);
	title(' Image reconstructed by S-FFT ');
	p=input('Display parameter (>1) (0=end) : ');
	if p==0,
		break
	end
end

