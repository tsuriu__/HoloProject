#!/usr/bin/octave 
%--------------------------LIM1-------------------------
% Function: Diffraction calculation using the S-FFT algorithm
% Procedure: consider an image file as the amplitude distribution of
% the initial plane, then calculate the amplitude of the diffracted field,
% giving a wavelength and a diffraction distance.
%
% Variables:
% h : wavelength (mm);
% z0 : diffraction distance (mm);
% U0 : complex amplitude of the initial field;
% L0 : width of the initial field (mm);
% Uf : complex amplitude of the field in the observation plane;
% L : width of the observation plane (mm);
% In case of a rectangular image, it is padded with zeros to its larger dimension
%------------------------------------------------------------
clear;close all;

[nom,chemin]=uigetfile('../images/');
[XRGB,MAP]=imread([chemin,nom]);
X=XRGB(:,:,1); % conserve the first channel if the image is RGB
h=input('Wavelength (mm) : ');
L0=input('Maximum width of the initial plane L0 (mm) : ');
k=2*pi/h;
[M,N]=size(X);
K=max(M,N);

% Zeros-padding to get KxK image

Z1=zeros(K,(K-N)/2);
Z2=zeros((K-M)/2,N);
Xp=[Z1,[Z2;X;Z2],Z1];
zmin=L0^2/K/h;
disp(['Minimum distance to fullfill sampling theorem : ',num2str(zmin),' mm']);
z0=input(['Diffraction distance z0 (mm) (>',num2str(zmin),'mm) : ']);
U0=double(Xp);


%--------------
n=1:K;m=1:K;
x=-L0/2+L0/K*(n-1);
y=-L0/2+L0/K*(m-1);
[xx,yy]=meshgrid(x,y);
Fresnel=exp(i*k/2/z0*(xx.^2+yy.^2));
f2=U0.*Fresnel;
Uf=fft2(f2,K,K);
Uf=fftshift(Uf);
L=h*abs(z0)*N/L0;
x=-L/2+L/K*(n-1);
y=-L/2+L/K*(m-1);
[xx,yy]=meshgrid(x,y);
phase=exp(i*k*z0)/(i*h*z0)*exp(i*k/2/z0*(xx.^2+yy.^2));
Uf=Uf.*phase;

%--------------
If=abs(Uf);% amplitude of the diffracted field

%--------------PlotingSection

figure(1)

subplot(1,2,1,'align');
imagesc(Xp),colormap(gray);axis('equal');axis('tight');ylabel('pixels');
xlabel(['Width of the initial plane: ',num2str(L0),' mm']);
title('Amplitude of the initial field');

subplot(1,2,2, 'align');
imagesc(abs(Uf)),colormap(gray);axis('equal');axis('tight');ylabel('pixels');
xlabel(['Width of the observation plane: ',num2str(L),'x',num2str(L),' mm']);
title('Amplitude of the image diffracted by S-FFT');

nom=strcat('../images/LIM1_',datestr(now,'DDmmYYHHMMSS'),'_',regexprep(nom,strsplit(nom,'.'){1,2},'.png'));
print(nom,'-dpng');


pause;

