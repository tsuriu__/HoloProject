#!/usr/bin/octave
%--------------------------LIM3-------------------------
% Function: simulate a hologram using an image whose width
% is less than a quarter of the initial plane.
% The simulated hologram will be saved with the name "Ih.tif".
%
% Procedure: (l) Read an image file
%
% Variables :
% h : wavelength (mm);
% Ih : hologram;
% L : width of the hologram (mm);
% L0 : width of the diffracted object field (mm);
% z0 : recording distance (mm);
% In case of a rectangular image, it is padded with zeros to its larger dimension
%------------------------------------------------------------
clear;close all;

[nom,chemin]=uigetfile('../images/');
[XRGB,MAP]=imread([chemin,nom]);

X=double(XRGB(:,:,1));% We recover the image of the red RGB

% band (channel 1)
[M,N]=size(X);

% Extended size to two times
K=2*max(N,M);

% Zeros-padding to get N N image
Z1=zeros(K,(K-N)/2);
Z2=zeros((K-M)/2,N);
Obj=[Z1,[Z2;X;Z2],Z1];

% Parameters
h=input('Wavelength (mm) : ');
k=2*pi/h;
L=input('Maximum width of the object (mm) : ');
z0=input(['Recording distance z0 (mm) : ']);
pix=abs(z0)*h/L;
Lx=K*pix;
Ly=K*pix;
disp(['Pixel pitch to fullfill sampling conditions : ',num2str(pix),'mm']);
disp(['Width of the object field = ',num2str(Lx),'mm x ',num2str(Ly),'mm']);

% Object field
psi=2*pi*(rand(K,K)-0.5);% Random phase
Ao=Obj.*exp(i.*psi); % Complex field in the object plane

figure;imagesc(Obj);colormap(gray);
colormap(gray);ylabel('pixels');
axis equal;axis tight;
xlabel(['Width of the object field = ',num2str(Lx),'mm x ',num2str(Ly),'mm']);
title('Initial Object');

%---------------Calculation using S-FFT

% Complex factor in the integral
n=-K/2:K/2-1;m=-K/2:K/2-1;
x=n*pix;y=m*pix;
[xx,yy]=meshgrid(x,y);
Fresnel=exp(i*k/2/z0*(xx.^2+yy.^2));
f2=Ao.*Fresnel;
Uf=fft2(f2,K,K);% Zero padding at KxK
Uf=fftshift(Uf);

% Complex factor in front of the integral
% Pitch in sensor plane
ipix=h*abs(z0)/K/pix;
xi=n*ipix;
yi=m*ipix;
L0x=K*ipix;
L0y=K*ipix;
[xxi,yyi]=meshgrid(xi,yi);
phase=exp(i*k*z0)/(i*h*z0)*exp(i*k/2/z0*(xxi.^2+yyi.^2));
Uf=Uf.*phase;

%--------------End of S-FFT calculation

disp(['Width of the diffracted field = ',num2str(L0x),'mm x ',num2str(L0y),'mm']);
figure,imagesc(abs(Uf)),colormap(gray);ylabel('pixels');
axis equal;axis tight;
xlabel(['Width of the diffracted field = ',num2str(L0x),'mm x ',num2str(L0y),'mm']);
title('Diffracted field in the detector plane (modulus)');
% Reference wave
ur=Lx/8/h/z0; % Spatial frequencies
vr=ur;
Ar=max(max(abs(Uf)));% Amplitude of the reference wave
Ur=Ar*exp(2*i*pi*(ur*xx+vr*yy));% Reference wave

%---------------Calculation of the hologram

H=abs(Ur+Uf).^2;

% 8-bit digitization
Imax=max(max(H));
Ih=uint8(255*H/Imax);
nom=strcat('../images/LIM3_',datestr(now,'DDmmYYHHMMSS'),'_',regexprep(nom,strsplit(nom,'.'){1,2},'tif'));
imwrite(Ih,nom);% Recording the hologram
disp(['Pixel pitch = ',num2str(ipix),' mm avec ',num2str(K),'X',num2str(K),' pixels']);
figure,imagesc(Ih),colormap(gray);ylabel('pixels');
xlabel(['Pixel pitch = ',num2str(ipix),' mm avec ',num2str(K),'X',num2str(K),'pixels']);
title(['Digital hologram with the name : ',nom]);

pause;
