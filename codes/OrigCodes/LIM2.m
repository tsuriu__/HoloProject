#!/usr/bin/octave
%--------------------------LIM2-------------------------
% Function: Diffraction calculation using the D-FFT algorithm
% Procedure: consider an image file as the optical amplitude
% distribution of the initial plane, the calculate the amplitude of the
% diffracted field, giving a wavelength and a diffraction distance
%
% Variables :
% h : wavelength (mm);
% z0 : diffraction distance (mm);
% U0 : complex amplitude of the initial optical field;
% L0 : width of the initial optical field and observation plane (mm);
% In case of a rectangular image, it is padded with zeros to its larger dimension
%------------------------------------------------------------
clear;close all;

chemin='.';

%[nom,chemin]=uigetfile([chemin,'*.*'],['initial image'],100,100);
[nom,chemin]=uigetfile('/home/tsuriu/Scripts/GitRpo/HoloProject');
[XRGB,MAP]=imread([chemin,nom]);
X=XRGB(:,:,1);
h=input('Wavelength (mm) : ');
L0=input('Maximum width of the initial field L0 (mm) : ');
k=2*pi/h;
[M,N]=size(X);
X=double(X);
K=max(M,N);

% Zeros-padding to get KxK image

Z1=zeros(K,(K-N)/2);
Z2=zeros((K-M)/2,N);
Xp=[Z1,[Z2;X;Z2],Z1];
zmax=L0^2/K/h;
disp(['Maximum distance to fullfill sampling theorem : ',num2str(zmax),' mm']);
z0=input(['Diffraction distance z0 (mm) (<',num2str(zmax),'mm) : ']);
U0=Xp;
figure(1),imagesc(Xp),colormap(gray);ylabel('pixels');
axis equal;axis tight;
xlabel(['Width of the initial field =',num2str(L0),' mm']);title('Initial amplitude ');

%---------------Diffraction calculation by D-FFT

Uf=fft2(U0,K,K);
Uf=fftshift(Uf); % Spectrum of the initial field
fex=K/L0;fey=fex;% sampling of frequency plane
fx=[-fex/2:fex/K:fex/2-fex/K];
fy=[-fey/2:fey/K:fey/2-fey/K];
[FX,FY]=meshgrid(fx,fy);
G=exp(i*k*z0*sqrt(1-(h*FX).^2-(h*FY).^2)); % Angular spectrum transfer function
% Diffraction
result=Uf.*G;
Uf=ifft2(result,K,K);

%---------------End of D-FFT calculation

If=abs(Uf);
figure(2),imagesc(abs(Uf)),colormap(gray);ylabel('pixels');
axis equal;axis tight;
xlabel(['Diffraction distance = ',num2str(z0),' mm, largeur du plan= ',num2str(L0),'mm']);title('Amplitude of field diffracted by D-FFT');

pause;
