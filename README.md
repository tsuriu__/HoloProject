LIM4 and LIM5 use a specific function 'mean2' from image package
To install the package, use the command bellow on octave prompt:
	pkg install image
After install the package, you must add a line to load it on code:
	pkg load image
